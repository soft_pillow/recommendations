from django.contrib import admin
from .models import Song,UserLog,Playlist,AlbumGroup,ArtistGroup,GenreGroup,Recommend,UserGroup,User_IB
admin.autodiscover()
admin.site.register(Song)
admin.site.register(Playlist)
admin.site.register(UserLog)
admin.site.register(ArtistGroup)
admin.site.register(AlbumGroup)
admin.site.register(GenreGroup)
admin.site.register(Recommend)
admin.site.register(UserGroup)
admin.site.register(User_IB)