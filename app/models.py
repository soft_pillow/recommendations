from django.db import models
from django.contrib.auth.models import User
from djangotoolbox.fields import ListField,EmbeddedModelField
# Create your models here.
'''
class Artist(models.Model):
	name= models.CharField(max_length=20)
	bio= models.TextField()
	artist_pic= models.FileField(upload_to="artist/",blank=True)

	def __unicode__(self):
		return self.name
	
	class Meta:
		ordering = ["name"]

class Album(models.Model):
	name= models.CharField(max_length=20)
	year= models.PositiveSmallIntegerField(blank=True, null=True)
	album_art= models.FileField(upload_to="album/",blank=True)

	def __unicode__(self):
		return "%s %s" %(self.name,self.year)
'''

class Song(models.Model):
	title= models.CharField(max_length=50)
	artist=models.CharField(max_length=50)
	album= models.CharField(max_length=50)
	year=models.PositiveSmallIntegerField(blank=True, null=True)
	genre= models.CharField(max_length=50)
	views=models.PositiveSmallIntegerField(default=0)
	file= models.FileField(upload_to="songs/")

	def __unicode__(self):
		return "%s-%s-%s-%s" % (self.title,self.album,self.artist,self.views)

	class Meta:
		ordering = ["-views"]

class ArtistGroup(models.Model):
	groupName=models.CharField(max_length=20)
	songs=models.ManyToManyField(Song)

	def __unicode__(self):
		return "%s %d" % (self.groupName,self.songs.count())


class AlbumGroup(models.Model):
	groupName=models.CharField(max_length=20)
	songs=models.ManyToManyField(Song)
	album_art= models.FileField(upload_to="album/",blank=True)


	def __unicode__(self):
		return "%s %d" % (self.groupName,self.songs.count())


class GenreGroup(models.Model):
	groupName=models.CharField(max_length=20)
	songs=models.ManyToManyField(Song)

	def __unicode__(self):
		return "%s %d" % (self.groupName,self.songs.count())


class Playlist(models.Model):
	user= models.ForeignKey(User)
	name= models.CharField(max_length=50)
	songs= models.ManyToManyField(Song, related_name="playlist")
	date= models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return "%s %s %s" % (self.name,self.user,self.date)

	class Meta:
		ordering = ["name"]



class UserLog(models.Model):
	song= models.ForeignKey(Song)
	user= models.ForeignKey(User)
	time= models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return "%s %s %s" % (self.user.username, self.time,self.song.title)

	class Meta:
		ordering = ["-time"]


class Dicty(models.Model):
    name=models.TextField()

class Recommend(models.Model):
	user=models.ForeignKey(User)
	artist_rec=models.ManyToManyField(Song, related_name="artist_rec")
	album_rec= models.ManyToManyField(Song, related_name="album_rec")
	genre_rec= models.ManyToManyField(Song, related_name="genre_rec")
	friend_rec= models.ManyToManyField(Song, related_name="friend_rec")
	col_rec= models.ManyToManyField(Song, related_name="col_rec")
	#following contains only one dict in list  list=[dict{}]
	artist_preference_table= models.TextField()
	album_preference_table= models.TextField()
	genre_preference_table= models.TextField()
	diff_table= models.TextField()

	def __unicode__(self):
		return self.user.username

class User_IB(models.Model):
	user= models.ForeignKey(User)
	ib_vector= models.ManyToManyField(Dicty, related_name="id_vector")

class UserGroup(models.Model):
	groupName=models.CharField(max_length=20)
	user_list= models.ManyToManyField(User, related_name="user_list")
