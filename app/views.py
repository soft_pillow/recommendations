# Create your views here.
from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse,Http404
from django.views.generic import TemplateView,View,DetailView
from app.forms import SongSearchForm
from .models import Playlist,Song,UserLog,Recommend,AlbumGroup
import json
from itertools import chain

class root(View):

	def get(self,request):
		form= SongSearchForm(request.GET)
		results= form.search()
		return render(request,'search/search.html',
			{
				'songs' :results
			})

class PlayNow(View):

	def get(self,request):
		if request.is_ajax():
			song=Song.objects.get(id=request.GET["songId"])

			song.views+=1
			song.save()
			log=UserLog(song=song,
						user=request.user)	
			log.save()
			pic= AlbumGroup.objects.get(groupName=song.album)
			data={ 'title':song.title,'url':song.file.url,'artist':song.artist,'view':song.views,
			'album_art':pic.album_art.url}

			return HttpResponse(json.dumps(data),content_type="application/json")

		else:
			raise Http404
		

class playlist(View):

	def post(self,request):
		playlist=Playlist(user=request.user,
						  name=request.POST['name'])
		playlist.save()
		return HttpResponseRedirect("/")


class home(View):

	def get(self,request):
		song=Song.objects.all()[:10]
		rec=Recommend.objects.get(user=request.user)
		rec_list=list(chain(rec.artist_rec.all(),rec.album_rec.all(),rec.genre_rec.all()))
		template_name="home.html"
		playlist=Playlist.objects.filter(user=request.user)
		usrlog= UserLog.objects.filter(user=request.user)
		return render(request,template_name,{"songs":song,
			"rec_list":rec_list,
			"playlists":playlist,
			"userlog":usrlog})