$(document).ready(function(){
	
    var sId;
	$("#player").hide();
    $("#all_btn").attr('class','btn btn-default active')
    $("#artist").hide();
    $("#album").hide();
    $("#genre").hide();
    $("#collab").hide();
	$("#success_result").hide()
	//Ajax GET play song
	function playSong(songId){	
		 $.ajax({
            type: "GET",
            url: "/play/",
            data: {"songId":songId},
            success: function(data) {
            	$("#sound").attr('src',data.url);
            	$("#title").html(data.title);
            	$("#pic").attr('src',data.album_art);
            	$("#views").html("Total Views "+data.view);
                $("#artist").html(data.artist);
            	$("#song").load();
            	//$("#song").play();
            	
            }
        });

	}

     $("#all_btn").click(function(){
        $("#artist").hide();
        $("#album").hide();
        $("#genre").hide();
        $("#collab").hide();
        $("#all").fadeIn();
    })
    
    $("#artist_btn").click(function(){
        $("#all_btn").attr('class','btn btn-default')
        $("#all").hide();
        $("#album").hide();
        $("#genre").hide();
        $("#collab").hide();
        $("#artist").fadeIn();
    })

     $("#album_btn").click(function(){
        $("#all_btn").attr('class','btn btn-default')
        $("#all").hide();
        $("#artist").hide();
        $("#genre").hide();
        $("#collab").hide();
        $("#album").fadeIn();
    })


     $("#genre_btn").click(function(){
        $("#all_btn").attr('class','btn btn-default')
        $("#all").hide();
        $("#artist").hide();
        $("#album").hide();
        $("#collab").hide();
        $("#genre").fadeIn();
    })

     $("#collab_btn").click(function(){
        $("#all_btn").attr('class','btn btn-default')
        $("#all").hide();
        $("#artist").hide();
        $("#album").hide();
        $("#genre").hide();
        $("#collab").fadeIn();
    })
 
 

	$("td").click(function(){	
		$("#userinfo").hide();
		sId=this.id
        playSong(sId);
		$("#player").fadeIn();	
	});


    $('[name="playlist_name"]').click(function(){

        plalist_id=this.id;
        console.log(plalist_id, sId);
        $.ajax({
            type: "GET",
            url: "/playlist/",
            data: {"songId":sId, "playlistId":plalist_id},
           
            success: function(data) {
                console.log(data.msg);
                $("#success_result").fadeIn();
            }

    });
	});

	// CSRF code
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});