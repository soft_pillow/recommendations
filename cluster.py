from django.contrib.auth.models import User
from app.models import UserLog,User_IB,UserGroup
from math import sqrt
from datetime import datetime,timedelta
import math
from random import choice
def root_mean_square(ib_vector,centroid):
	sum=0.0
	for i in range(0,len(ib_vector)):
		sum+= (ib_vector[i]-centroid[i])**2

	return math.sqrt(sum)

def group_scatterdness(user_group):
	no_users=len(user_group)
	centroid=[]
	ib_length=len(user_group[0].ib_vector)
	
	#calculate centroid 
	for i in range(0,ib_length):
		temp=0.0
		
		for u in user_group:
			temp+=u.ib_vector[i]
		
		centroid.append(temp/no_users)	
	sum=0.0
	for u in user_group:
		sum+=root_mean_square(u.ib_vector,centroid)

	return sum/no_users,centroid

def find_max_scattered(group_list):
	max_scatter=0
	pos=0
	for i in range(0,len(group_list)):
		if group_list[i].scatterdness>max_scatter:
			max_scatter=group_list[i].scatterdness
			pos=i

	return group_list[pos]

def find_nearest(userA,userB,usr):
	distA=root_mean_square(userA.ib_vector,usr.ib_vector)
	distB=root_mean_square(userB.ib_vector,usr.ib_vector)

	if distA<distB:
		return "A"
	else:
		return "B"

def cluster():
	user_list=User_IB.objects.all()
	no_users= len(user_list)
	no_of_groups= int(sqrt(no_users))
	ref_table={}
	group_list=[]
	scatter,centroid=group_scatterdness(user_list)
	first_group=UserGroup(user_list=user_list,
						no_of_users=no_users,
						centroid=centroid,
						scatterdness=scatter)

	group_list.append(first_group)
	print "BEFORE FOR LOOP"
	print "Group list:"
	for g in group_list:
		print g.user_list
	#print math.sqrt(no_users-1)
	print "ENTERING FOR LOOP:"
	for i in range(0,int(round(math.sqrt(no_users))-1)):
		max_scattered_group=find_max_scattered(group_list)
		print "max_scattered_group:",max_scattered_group.user_list
		userA= choice(max_scattered_group.user_list)
		print "UserA:",userA
		remaining_users=list(set(max_scattered_group.user_list) - set([userA]))
		print "remaining_users",remaining_users
		userB= choice(remaining_users)
		print "UserB:",userB
		remaining_users=list(set(remaining_users) - set([userB]))
		print "remaining_users",remaining_users

		groupA=UserGroup(user_list=[userA],
						no_of_users=1)

		groupB=UserGroup(user_list=[userB],
						no_of_users=1)

		for usr in remaining_users:
			choosen=find_nearest(userA,userB,usr)

			if choosen=="A":
				groupA.user_list.append(usr)
				groupA.no_of_users+=1
			else:
				groupB.user_list.append(usr)
				groupB.no_of_users+=1
		

		s1,c1=group_scatterdness(groupA.user_list)
		groupA.centroid=c1
		groupA.scatterdness=s1

		s2,c2=group_scatterdness(groupB.user_list)
		groupB.centroid=c2
		groupB.scatterdness=s2

		group_list.remove(max_scattered_group)
		group_list.append(groupA)
		group_list.append(groupB)
	
	print "PRINTING THE GROUPS"
	for group in group_list:
		if group.no_of_users>1:
			group.save()
			print "user_list:",group.user_list,
			print "no_of_users:",group.no_of_users
			print "centroid:",group.centroid
			print "scatterdness:",group.scatterdness

