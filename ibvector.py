from django.contrib.auth.models import User
from app.models import UserLog,ArtistGroup,User_IB
from math import sqrt
from datetime import datetime,timedelta

today= datetime.now()

def find_ib_vector(usr,day_log):
	transaction_table={ 1:[],
						2:[],
						3:[],
						4:[],
						5:[]
						}

	interest_table={}
	behavior_table={}
	ib_vector=[]
	#populate transaction table
	for i in range(1,16):
		temp_transac=transaction_table[int((i+2)/3)]
		#print "day_log: ",day_log[i-1].song.artist
		if temp_transac.count(day_log[i-1].song.artist)==0:
			temp_transac.append(day_log[i-1].song.artist)
		
		'''
		 #   Not to use because too many groups in transaction_table 
		 #	will increase the runtime complexity
		 #  hence restrincting only to music groups by ARTIST

		if temp_transac.count(day_log[i-1].song.album)==0:
			temp_transac.append(day_log[i-1].song.album)
		

		if temp_transac.count(day_log[i-1].song.genre)==0:
			temp_transac.append(day_log[i-1].song.genre)
			
		'''
	#adding keys with black dict to interest_table={ 'a'={'c':0,'ft':0,'lt':0},'b':{}}
	
	print "transaction_table:",transaction_table
	for k,v in transaction_table.iteritems():
		for i in v:
			interest_table[i]={'c':0.0,'ft':0.0,'lt':0.0,'s':0.0}


	#calculating count and last transaction
	for k,v in transaction_table.iteritems():
		for i in v:
			interest_table[i]['c']+=1
			interest_table[i]['lt']=k

	# first trans 
	for k in sorted(transaction_table.keys(),reverse=True):
		for v in transaction_table[k]:
			interest_table[v]['ft']=k

	#calcualting support count
	
	for k,v in interest_table.iteritems():
		v['s']=v['c']/(5-v['ft']+1)
	
	
	for k in interest_table.items():
		if k[1]['s']<0.5:
			interest_table.pop(k[0])

	print "interest_table: ",interest_table	
	# behavior_table={ (a,b):{'c':0,'ft':0,'lt':0},'b':{}, (b,c):{} }
	for i in interest_table:
		for j in interest_table:
			print "guruaj"
			if i!=j:
				print "true"
				behavior_table[(i,j)]={'c':0.0,'ft':0.0,'lt':0.0,'s':0.0}

	
	#calculating count and last transaction in behavior_table
	for k,v in behavior_table.iteritems():
		for i,j in transaction_table.iteritems():
			if k[0] in j and k[1] in j:
				v['c']+=1
				v['lt']=i

	#calculation first count for behavior_table
	for k,v in behavior_table.iteritems():
		for i in sorted(transaction_table.keys(),reverse=True):
				if k[0] in transaction_table[i] and k[1] in transaction_table[i]:
					v['ft']=i

	#calculating support count
	for k,v in behavior_table.iteritems():
		v['s']=v['c']/(5-v['ft']+1)

	
	for k in behavior_table.items():
		if k[1]['s']<0.5:
			behavior_table.pop(k[0])

	print "behavior_table: ",behavior_table
	#IB vector
	artist_all=ArtistGroup.objects.all()
	
	print "artist_all: ",artist_all
	#generating ib_vector=[0, 0, 1, 1, 1, 1, 0, 0, 1, 1]
	for i in range(0,len(artist_all)):
		for j in range(i,len(artist_all)):
			if i==j:
				if artist_all[i].groupName in interest_table.keys():
					ib_vector.append(1)
				else:
					ib_vector.append(0)
			elif (artist_all[i].groupName,artist_all[j].groupName) in behavior_table.keys() or (artist_all[j].groupName,artist_all[i].groupName) in behavior_table.keys():
				ib_vector.append(1)
			else:
				ib_vector.append(0)

	return ib_vector




def ib_vector():
	user_list=User.objects.all()

	for usr in user_list:
		#each user must have 15 songs
		day_log=UserLog.objects.filter(user=usr)[:15]
		print "FOR USER:",usr.username," LOG LENGTH:",len(day_log)
		if len(day_log)>=10:
			IB_vector=find_ib_vector(usr,day_log)
			print IB_vector
			#may throw error check for correct method to override ListField()
			u=User_IB.objects.get_or_create(user=usr)
			u[0].ib_vector=IB_vector
			u[0].save()

			print "saved",usr

	pass
