from app.models import UserLog,User_IB,UserGroup,Recommend,ArtistGroup
from cluster import cluster
from datetime import datetime,timedelta
import random
import json
import ast

today= datetime.now()
month_ago= today- timedelta(days=30)

def calculate_ref_table(user_list):
	artist_list=[]

	for usr in user_list:
		temp_user= Recommend.objects.get(user=usr)
		pref_table= ast.literal_eval(temp_user.artist_preference_table)
		artist_list=list(set(artist_list) | set(pref_table.keys()))

	artist_count=len(artist_list)

	total_table={}
	for v in artist_list:
		total_table[v]=0.0

	for usr in user_list:
		temp_user= Recommend.objects.get(user=usr)
		pref_table=ast.literal_eval(temp_user.artist_preference_table)

		for k,v in pref_table.iteritems():
			total_table[k]+=v

	return [total_table]


def calculate_dif_table(usr,grp):
	temp_recommend=Recommend.objects.get(user=usr)
	pref_table= ast.literal_eval(temp_recommend.artist_preference_table)
	total_table= grp.total_table[0]
	ref_table={}
	diff_table={}
	for k,v in total_table.iteritems():
		temp=0.0
		if pref_table.has_key(k):
			temp=pref_table[k]	
		ref_table[k]=(v-temp)/(grp.user_list.count())
		diff_table[k]=ref_table[k]-temp

	for k in diff_table.items():
		if k[1]<=0:
			diff_table.pop(k[0])


	temp_recommend.diff_table=diff_table
	print "diff_table:",diff_table
	temp_recommend.save()	

	pass


def find_col_rec(usr):
	actual_user= Recommend.objects.get(user=usr)
	diff_table=ast.literal_eval(actual_user.diff_table)
	value=diff_table.values()
	total_wieght=sum(value)
	log_song_list=[]
	
	original_diff_table=diff_table.copy()
	
	user_log=UserLog.objects.filter(user=actual_user).filter(time__range=(month_ago,today))
	


	for s in user_log:
		log_song_list.append(s.song)

	col_rec_list=[]
	
	for i in diff_table:
		diff_table[i]=int(diff_table[i]*10/total_wieght)
		print i
		temp=ArtistGroup.objects.get(groupName=i)
		temp_list=temp.songs.all()
		col_all_songs=set(temp_list)-set(log_song_list)
		del_len=len(col_all_songs)-diff_table[i]
		col_all_songs=[s for s in col_all_songs]
		for i in range(0,del_len):
			col_all_songs.remove(random.choice(col_all_songs))

		col_rec_list+=col_all_songs


	print col_rec_list, diff_table
	return col_rec_list


def collaborative_rec():
	group_list= UserGroup.objects.all()

	#cluster()

	for g in group_list:
		rt=calculate_ref_table(g.user_list.all())
		g.total_table=rt
		print "Total_table:",g.total_table
		#g.save()

	for grp in group_list:
		for usr in grp.user_list.all():
			calculate_dif_table(usr,grp)
			col_rec_list=find_col_rec(usr)
			temp_user=Recommend.objects.get(user=usr)
			temp_user.col_rec=col_rec_list
			temp_user.save()





