from django.contrib.auth.models import User
from app.models import UserLog,Song,ArtistGroup,AlbumGroup,GenreGroup,Recommend
from datetime import datetime,timedelta
import random

today= datetime.now()
month_ago= today- timedelta(days=30)


def rec_by_artist(user_log):

	preference_table={}
	for log in user_log:
		time_diff=today-log.time
		'''
		weight of today is 40
		weight of n days back is 40-n
		divide weight by 40 to convert to range 0-1
		'''
		weight=(40-(time_diff.days))/40

		if preference_table.has_key(log.song.artist):
			preference_table[log.song.artist]+=weight
		else:
			preference_table[log.song.artist]=weight			

	#end of for
	value=preference_table.values()
	total_wieght=sum(value) or 1
	log_song_list=[]
	
	artist_preference_table=preference_table.copy()
	
	for s in user_log:
		log_song_list.append(s.song)

	artist_rec_list=[]
	
	for i in preference_table:
		preference_table[i]=int(preference_table[i]*10/total_wieght)
		print i
		temp=ArtistGroup.objects.get(groupName=i)
		temp_list=temp.songs.all()
		artist_all_songs=list(set(temp_list)-set(log_song_list))
		del_len=len(artist_all_songs)-preference_table[i]

		for i in range(0,del_len):
			artist_all_songs.remove(random.choice(artist_all_songs))

		artist_rec_list+=artist_all_songs


	return artist_rec_list,artist_preference_table

	

def rec_by_album(user_log):
	preference_table={}
	for log in user_log:
		time_diff=today-log.time
		print "time: ",time_diff
		'''
		weight of today is 40
		weight of n days back is 40-n
		divide weight by 40 to convert to range 0-1
		'''
		weight=(40-(time_diff.days))/40

		if preference_table.has_key(log.song.album):
			preference_table[log.song.album]+=weight
		else:
			preference_table[log.song.album]=weight			

	#end of for
	print preference_table
	value=preference_table.values()
	total_wieght=sum(value) or 1
	log_song_list=[]
	
	album_preference_table=preference_table.copy()
	
	for s in user_log:
		log_song_list.append(s.song)

	album_rec_list=[]
	
	for i in preference_table:
		preference_table[i]=int(preference_table[i]*10/total_wieght)
		print i
		try:
			temp=AlbumGroup.objects.get(groupName=i)
			temp_list=list(temp.songs)
			album_all_songs=list(set(temp_list)-set(log_song_list))
			del_len=len(album_all_songs)-preference_table[i]

			for i in range(0,del_len):
				album_all_songs.remove(random.choice(album_all_songs))

			album_rec_list+=album_all_songs
		except:
			print "Error with ",i

	print album_rec_list, album_preference_table
	return album_rec_list,album_preference_table
	

def rec_by_genre(user_log):
	preference_table={}
	for log in user_log:
		time_diff=today-log.time
		print "time: ",time_diff
		'''
		weight of today is 40
		weight of n days back is 40-n
		divide weight by 40 to convert to range 0-1
		'''
		weight=(40-(time_diff.days))/40

		if preference_table.has_key(log.song.genre):
			preference_table[log.song.genre]+=weight
		else:
			preference_table[log.song.genre]=weight			

	#end of for
	print preference_table
	value=preference_table.values()
	total_wieght=sum(value) or 1
	log_song_list=[]
	
	genre_preference_table=preference_table.copy()
	
	for s in user_log:
		log_song_list.append(s.song)

	genre_rec_list=[]
	
	for i in preference_table:
		preference_table[i]=int(preference_table[i]*10/total_wieght)
		print i
		temp=GenreGroup.objects.get(groupName=i)
		temp_list=temp.songs.all()
		genre_all_songs=list(set(temp_list)-set(log_song_list))
		del_len=len(genre_all_songs)-preference_table[i]

		for i in range(0,del_len):
			genre_all_songs.remove(random.choice(genre_all_songs))

		genre_rec_list+=genre_all_songs


	print genre_rec_list, genre_preference_table
	return genre_rec_list,genre_preference_table
	pass



def rec_all():
	all_usr=User.objects.all()
	for usr in all_usr:
		
		new_rec, _=Recommend.objects.get_or_create(user=usr)
		print usr
		user_log=UserLog.objects.filter(user=usr).filter(time__range=(month_ago,today))
	
		artist_song_list,artist_preference_table= rec_by_artist(user_log) 
		new_rec.artist_rec=artist_song_list
		new_rec.artist_preference_table=artist_preference_table
		print "end of artist_rec"
		
		album_song_list,album_preference_table=rec_by_album(user_log)
		new_rec.album_rec=album_song_list
		new_rec.album_preference_table=album_preference_table
		print "end of album_rec"
		genre_song_list,genre_preference_table=rec_by_genre(user_log)
		new_rec.genre_rec=genre_song_list
		new_rec.genre_preference_table=genre_preference_table
		print "end of genre_rec"
		new_rec.save()
		#final rec
		
		
		
		
		
		

		
		



