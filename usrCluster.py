from django.contrib.auth.models import User
from app.models import UserLog,ArtistGroup,User_IB
from math import sqrt
from datetime import datetime,timedelta

today= datetime.now()

def find_ib_vector(usr,day_log):
	transaction_table={ 1:[],
						2:[],
						3:[],
						4:[],
						5:[]
						}

	interest_table={}
	behavior_table={}
	ib_vector=[]
	#populate transaction table
	for i in range(1,11):
		temp_transac=transaction_table[int((i+1)/2)]
		
		if temp_transac.count(day_log[i-1].song.artist)==0:
			temp_transac.append(day_log[i-1].song.artist)
		
		'''
		 #   Not to use because too many groups in transaction_table 
		 #	will increase the runtime complexity
		 #  hence restrincting only to music groups by ARTIST

		if temp_transac.count(day_log[i-1].song.album)==0:
			temp_transac.append(day_log[i-1].song.album)
		

		if temp_transac.count(day_log[i-1].song.genre)==0:
			temp_transac.append(day_log[i-1].song.genre)
			
		'''
	#adding keys with black dict to interest_table={ 'a'={'c':0,'ft':0,'lt':0},'b':{}}
	
	for k,v in transaction_table.iteritems():
		for i in v:
			interest_table[i]={'c':0,'ft':0,'lt':0,'s':0}


	#calculating count and last transaction
	for k,v in transaction_table.iteritems():
		for i in v:
			interest_table[i]['c']+=1
			interest_table[i]['lt']=k

	# first trans 
	for k in sorted(transaction_table.keys(),reverse=True):
		for v in transaction_table[k]:
			interest_table[v]['ft']=k

	#calcualting support count
	for k,v in interest_table.iteritems():
		v['s']=v['c']/(5-v['ft']+1)
		
		if v['s']<0.65:
			del interest_table[k]

	# behavior_table={ (a,b):{'c':0,'ft':0,'lt':0},'b':{}, (b,c):{} }
	for i in interest_table:
		for j in interest_table:
			if i!=j:
				behavior_table[(i,j)]={'c':0,'ft':0,'lt':0,'s':0}

	#calculating count and last transaction in behavior_table
	for k v in behavior_table.iteritems():
		for i,j in transaction_table.iteritems():
			if k[0] in j and k[1] in j:
				v['c']+=1
				v['lt']=i

	#calculation first count for behavior_table
	for k,v in behavior_table.iteritems():
		for i in sorted(transaction_table.keys(),reverse=True):
				if k[0] in transaction_table[i] and k[1] in transaction_table[i]:
					v['ft']=i

	#calculating support count
	for k,v in behavior_table.iteritems():
		v['s']=v['c']/(5-v['ft']+1)

		if v['s']<0.65:
			del behavior_table[k]

	#IB vector
	artist_all=ArtistGroup.objcects.all()
	
	#generating ib_vector=[0, 0, 1, 1, 1, 1, 0, 0, 1, 1]
	for i in range(0,len(artist_all)):
		for j in range(i,len(artist_all)):
			if i==j:
				if artist_all[i] in interest_table.keys():
					ib_vector.append(1)
				else:
					ib_vector.append(0)
			elif (artist_all[i],artist_all[j]) in behavior_table.keys() or (artist_all[j],artist_all[i]) in behavior_table.keys():
				ib_vector.append(1)
			else:
				ib_vector.append(0)

	return ib_vector




def ib_vector():
	user_list=User.objcects.all()

	for usr in user_list:
		day_log=UserLog.objcects.filter(user=usr)[:10]
		if len(day_log)>=10:
			IB_vector=find_ib_vector(usr,day_log)
			#may throw error check for correct method to override ListField()
			User_IB.objcects.get_or_create(user=usr,ib_vector=IB_vector).save()


	pass


def cluster():
	user_list=User.objcects.all()
	no_users= len(user_list)
	no_of_groups= int(sqrt(no_users))


